#A Flask application that provides a FizzBuzz implementation with customizable parameters.

from flask import Flask, jsonify, request, current_app

app = Flask(__name__)
app.config['STATS'] = {}


def generate_fizzbuzz(int1, int2, limit, str1, str2):
    """
        Generate a FizzBuzz sequence based on the provided parameters.

        Parameters:
        -----------
            int1 : int
                First integer to determine the 'Fizz' condition. Multiples of this integer will be replaced with 'str1'.
                
            int2 : int
                Second integer to determine the 'Buzz' condition. Multiples of this integer will be replaced with 'str2'.
                
            limit : int
                Upper limit for generating the FizzBuzz sequence.
                
            str1 : str
                String representation for replacing multiples of 'int1'.
                
            str2 : str
                String representation for replacing multiples of 'int2'.

        Returns:
        --------
            list
                A list containing the FizzBuzz sequence. Each element in the list corresponds to numbers from 1 to 'limit'.
                - Multiples of 'int1' are replaced by 'str1'.
                - Multiples of 'int2' are replaced by 'str2'.
                - Multiples of both 'int1' and 'int2' are replaced by the concatenation of 'str1' and 'str2'.
    """

    fizzbuzz_list = [
        str1 + str2 if i % int1 == 0 and i % int2 == 0 else
        str1 if i % int1 == 0 else
        str2 if i % int2 == 0 else
        str(i)
        for i in range(1, limit + 1)
    ]
    return fizzbuzz_list

@app.route('/fizzbuzz/', methods=['GET'])
def fizzbuzz():
    """
        Generate a FizzBuzz sequence based on the provided query parameters from the request.

        This function retrieves the values for 'int1', 'int2', 'limit', 'str1', and 'str2' from the request arguments.
        It then validates these parameters and uses them to generate the FizzBuzz sequence using the helper function 'generate_fizzbuzz'.

        Returns:
        --------
            JSON Response
                A JSON response containing the FizzBuzz sequence or an error message.
                - If all parameters are valid, it returns a JSON response with the FizzBuzz sequence up to the specified limit.
                - If any parameter is missing or invalid, it returns a JSON error response with an appropriate message.

        Example Usage:
        --------------
            To use this function, make a GET request to the endpoint with the required query parameters:
            http://127.0.0.1:5000/fizzbuzz/?int1=<int1_value>&int2=<int2_value>&limit=<limit_value>&str1=<str1_value>&str2=<str2_value>

            For instance:
            http://127.0.0.1:5000/fizzbuzz/?int1=3&int2=5&limit=15&str1=Fizz&str2=Buzz

            This will generate a FizzBuzz sequence up to 15 where multiples of 3 are replaced by 'Fizz', multiples of 5 are replaced by 'Buzz', and multiples of both 3 and 5 are replaced by 'FizzBuzz'.
    """   
    try:
        result = []
        int1 =  int(request.args.get('int1'))
        int2 =  int(request.args.get('int2'))
        limit =  int(request.args.get('limit'))
        str1 =  str(request.args.get('str1'))
        str2 =  str(request.args.get('str2'))

        params = (int1, int2, limit, str1, str2)
        current_stats = current_app.config['STATS']

        # Update statistics
        current_stats[params] = current_stats.get(params, 0) + 1
        current_app.config['STATS'] = current_stats

        if not all([int1, int2, limit, str1, str2]):
            return jsonify({"error": "All parameters are required and must be valid."}), 400

        fizzbuzz_result = generate_fizzbuzz(int1, int2, limit, str1, str2)
        return jsonify({"result": fizzbuzz_result})

    except ValueError:
        return jsonify({"error": "Invalid parameter type. Ensure int1, int2, and limit are integers."}), 400

@app.route('/statistics/', methods=['GET'])
def statistics():
    """
        Retrieve statistics on the most frequently used request parameters from the application's configuration.

        This block of code fetches the statistics on the most commonly used request parameters stored in the application's configuration.
        It identifies the parameters that have been used most frequently and returns these along with their hit counts.

        Parameters:
        -----------
            None

        Returns:
        --------
            JSON Response
                A JSON response containing details of the most frequently used request parameters and their hit counts.

                Example Response:
                {
                    "most_used_request": {
                        "int1": <most_frequent_int1_value>,
                        "int2": <most_frequent_int2_value>,
                        "limit": <most_frequent_limit_value>,
                        "str1": "<most_frequent_str1_value>",
                        "str2": "<most_frequent_str2_value>",
                        "hits": <number_of_hits_for_most_frequent_request>
                    }
                }

        Note:
        -----
            If there are no statistics available (i.e., no requests have been made or the statistics have been reset), 
            the function returns default values with zero hits for all parameters.
    """

    current_stats = current_app.config['STATS']
    
    # Find the most frequent request parameters
    most_frequent_params = max(current_stats, key=current_stats.get) if current_stats else None
    hits = current_stats.get(most_frequent_params, 0) if most_frequent_params else 0
    
    return jsonify({
        "most_used_request": {
            "int1": most_frequent_params[0],
            "int2": most_frequent_params[1],
            "limit": most_frequent_params[2],
            "str1": most_frequent_params[3],
            "str2": most_frequent_params[4],
            "hits": hits
        }
    })


if __name__ == '__main__':
    app.run(debug=True)